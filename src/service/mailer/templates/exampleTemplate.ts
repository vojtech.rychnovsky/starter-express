interface IParams {
  headline: string
  paragraph: string
}

export const renderTemplate = ({ headline, paragraph }: IParams): string =>
  `<!doctype html>
  <html>
    <head>
      <meta name="viewport" content="width=device-width" />
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>${headline}</title>
    </head>
    <body>
      <p>${paragraph}</p>
    </body>
  </html>
  `
