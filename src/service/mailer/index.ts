import { createTransport, SentMessageInfo } from 'nodemailer'
import { Options } from 'nodemailer/lib/mailer'
import config from '../../config'

// define gmail server credentials
const gmailPassword = config.GMAIL_PASSWORD
const gmailUsername = config.GMAIL_USERNAME

const replyToMail = config.EMAIL_REPLY_TO

export const sentMail = async (options: Options): Promise<SentMessageInfo> => {
  const mailTransport = createTransport({
    service: 'gmail',
    auth: {
      user: gmailUsername,
      pass: gmailPassword,
    },
  })

  const opt: Options = {
    cc: replyToMail,
    from: `Pingl payments <${gmailUsername}>`,
    replyTo: replyToMail,
    ...options,
  }

  console.log(
    `Sending mail to ${opt.to} in copy to ${opt.cc} with subject ${opt.subject}`
  )

  return await mailTransport.sendMail(opt)
}
