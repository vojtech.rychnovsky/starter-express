import { Request } from 'express'
import { asyncMiddleware } from '../utils/expressUtils'

export const exampleAction = asyncMiddleware((request: Request) => {
  console.log(request.body)
  return 'OK'
})
