import { Request, Response } from 'express'

const AUTH_TOKEN = 'xxxxxx'

type Controller = (
  req: Request
) => {} | string | void | Promise<{} | string | void>

const asyncMiddleware = (fn: Controller) => async (
  req: Request,
  res: Response
) => {
  try {
    const value = await fn(req)
    console.info('DONE', value)
    res.status(200).send(value)
  } catch (err) {
    console.info('FAILED')
    console.error(err)
    res.status(500).send(err.message)
  }
}

// Wrapper around contollers with auth
const asyncMiddlewareProtected = (fn: Controller) => async (
  req: Request,
  res: Response
) => {
  if (req.headers.authorization !== `Bearer ${AUTH_TOKEN}`) {
    res.status(401).send()
    return
  }

  await asyncMiddleware(fn)(req, res)
}

export { Controller, asyncMiddleware, asyncMiddlewareProtected }
