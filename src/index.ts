import * as cors from 'cors'
import { Express, Request, Response } from 'express'
import exampleRouter from './routes/exampleRouter'
import config from './config'

import express = require('express')

let app: Express

try {
  console.log(`Starting server env: ${config.env}`)

  // app setup
  app = express()
  app.use(cors())

  // test route with app version
  app.get('/', (req, res) => {
    res.status(200).send(`v${config.REACT_APP_VERSION}`)
  })

  // delegate to routers
  app.use('/example', exampleRouter)
} catch (err) {
  console.error(err)
}

// define top level cloud functions
// to be called by gcloud
// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const api = (req: Request, res: Response) => {
  if (!req.url) {
    req.url = '/'
    req.path = '/'
  }
  return app(req, res)
}
