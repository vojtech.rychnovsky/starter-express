import * as exampleController from '../controllers/exampleController'

import express = require('express')

/**
 * Router for administration part of the backend
 */
const router = express.Router()

router.post('/subroute', exampleController.exampleAction)

export default router
