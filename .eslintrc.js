// .eslintrc.js
'use strict'

module.exports = {
  extends: [
    '@strv/eslint-config-node/v10',
    '@strv/eslint-config-node/optional',
    '@strv/eslint-config-typescript',
    '@strv/eslint-config-typescript/style',
    'prettier',
  ],

  parserOptions: {
    // The project field is required in order for some TS-syntax-specific rules to function at all
    // @see https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/parser#configuration
    project: './tsconfig.json',
  },

  rules: {
    'no-console': 'off',
    'new-cap': 'off',
    '@typescript-eslint/no-require-imports': 'off',
    'padding-line-between-statements': 'off',
  },
}
