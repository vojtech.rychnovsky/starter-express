module.exports = {
  testEnvironment: 'node',
  moduleFileExtensions: ['ts', 'tsx', 'js'],
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest',
  },
  globals: {
    'ts-jest': {
      tsConfig: 'tsconfig.json',
    },
  },
  setupFiles: ['./test/setupTests.ts'],
  testMatch: ['**/test/**/*.+(ts|tsx|js)'],
  testPathIgnorePatterns: ['setupTests.ts'],
}
