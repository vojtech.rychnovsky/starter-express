# Project description

This project contains serverless functions for Gifmemore backend. Functionality is following:

- Serverless API

# Setup

Project is running on Google Cloud Functions and is managed by serverless framework.

Intro to serverless framework: [Google - install](https://serverless.com/framework/docs/providers/google/guide/installation/)

Long story short, just install serverless and you are ready to go: `npm install -g serverless`

# Deploy

Project is written in TypeScript which needs to be transpiled to JS.

**Based on environments:**

- dev: `npm run deploy:dev`
- staging: `npm run deploy:staging`
- prod: `npm run deploy:prod`

## Deploy locally

It's possible to run functions locally using nodejs emulator. It's packet in tool `functions` from Google.

Install `npm install -g serverless`

Run locally: `npm run start`

After that you can invoke function by hitting local host url.

## Attach debugger

In `.vscode/settings.json` insert `{debug.node.autoAttach": "off"}`

Run locally: `npm run deploy`

Nodejs debugger will be attached automatically.

# Test

There are tests running on `jest`. To run them call: `npm run test`

# Google cloud services

This project is running on GCP and is build in a way that should fit into free usage quotas.

## Projects

There are multiple projects, each for different deployment environment.

- prod
- dev

## Cloud functions

Code is deployed to cloud functions by serverless framework tools.
